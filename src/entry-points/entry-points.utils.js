export function importAll(r) {
  r.keys().forEach(r);
}

// export function importAllEx(dirList, useSubDirs, regExp) {
//   if (typeof dirList === 'string') {
//     importAll(require.context(dirList, useSubDirs, regExp));
//   } else {
//     for (const dir of dirList)
//       importAll(require.context(dir, useSubDirs, regExp));
//   }
// }