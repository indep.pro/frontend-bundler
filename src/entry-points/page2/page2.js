import '../../pages/page2/page2';

import '../../favicons/favicons';

import { importAll } from '../entry-points.utils';

importAll(require.context('../../pages', true, /\.(png|jpe?g|gif|svg|webp)$/));
importAll(require.context('../../components', true, /\.(png|jpe?g|gif|svg|webp)$/));

importAll(require.context('../../fonts', true, /\.css$/));

importAll(require.context('../../components/', true, /\.*(scss|js)$/));

importAll(require.context('../../pages', true, /\.(mp4|m4v|ogg|flv|webm|asf|avi)$/));
importAll(require.context('../../components', true, /\.(mp4|m4v|ogg|flv|webm|asf|avi)$/));

importAll(require.context('../../pages', true, /\.(mp3|wav|aac|ogg|caf|flac)$/));
importAll(require.context('../../components', true, /\.(mp3|wav|aac|ogg|caf|flac)$/));
