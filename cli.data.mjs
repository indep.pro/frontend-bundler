function definePug(name) {
  return `include ../../components/header/header
include ../../components/logo/logo
include ../../components/footer/footer

mixin ${name}({text})
  .${name}
    +header
    main.${name}__main-content #{text}
    +footer
`;
}

function defineSCSS(block) {
  return `*,
*::before,
*::after {
  @include reset;
}

.${block} {
  display: flex;
  flex-direction: column;
  min-height: 100vh;
  background: wheat;      
  font-size: 26px;

  &__main-content {
    flex: 1;
    padding: 10px;
  }
}    
`;
}

function defineJS(name) {
  return `import './${name}.scss';`;
}

function defineEntryPointPug(relativePathToPage) {
  const segments = relativePathToPage.split('/');
  const name = segments[segments.length - 1];

  return `extends ../../template

include ${relativePathToPage}
  
block variables
  - let lang = "ru"
  - let title = ""
  - let description = ""
  - let keywords = ""
  
block content
  +${name}({
    text: 'This is ${name} page'
})
`;
}

function defineEntryPointJS(relativePathToPage) {
  return `import '${relativePathToPage}';

import '../../favicons/favicons';

import { importAll } from '../entry-points.utils';

importAll(require.context('../../pages', true, /\\.(png|jpe?g|gif|svg|webp)$/));
importAll(require.context('../../components', true, /\\.(png|jpe?g|gif|svg|webp)$/));

importAll(require.context('../../fonts', true, /\\.css$/));

importAll(require.context('../../components/', true, /\\.*(scss|js)$/));

importAll(require.context('../../pages', true, /\\.(mp4|m4v|ogg|flv|webm|asf|avi)$/));
importAll(require.context('../../components', true, /\\.(mp4|m4v|ogg|flv|webm|asf|avi)$/));

importAll(require.context('../../pages', true, /\\.(mp3|wav|aac|ogg|caf|flac)$/));
importAll(require.context('../../components', true, /\\.(mp3|wav|aac|ogg|caf|flac)$/));
`;
}

function defineComponentPug(block) {
  return `mixin ${block}
  .${block} Component ${block}
`;
}

function defineComponentSCSS(block) {
  return `.${block} {
  display: inline-block;  
  outline: 1px dashed gray;
}
`;
}
export {
  definePug,
  defineSCSS,
  defineJS,
  defineEntryPointPug,
  defineEntryPointJS,
  defineComponentPug,
  defineComponentSCSS,
};
