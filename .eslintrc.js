module.exports = {
  'env': {
    browser: true,
    es2021: true,
    jquery: true,
    node: true
  },
  'plugins': ['only-warn'],
  'extends': 'eslint:recommended',
  'parserOptions': {
    'ecmaVersion': 13,
    'sourceType': 'module'
  },
  'rules': {
    'indent': [
      'warn',
      2
    ],
    'quotes': [
      'warn',
      'single'
    ],
    'linebreak-style': 0,
    'class-methods-use-this': 0,
    'no-new': 0,
    'no-underscore-dangle': 0,
    'semi': 0,
    'camelcase': 0,
    'space-before-function-paren': 0,
    'no-mixed-operators': 0,
    'no-unused-vars': 0
  }
};
