#!/usr/bin/env node

//To use this tool just type in command line: node cli

import inquirer from 'inquirer';
import fs from 'fs';
import {
  definePug, defineSCSS, defineJS,
  defineEntryPointPug, defineEntryPointJS,
  defineComponentPug, defineComponentSCSS } from './cli.data.mjs';
import glob from 'glob';
import path from 'path';

chooseEntiryForCreate().then(({ entity }) => {
  if (entity === 'component') {
    inputComponentName().then(({ componentName }) => {
      inputSubDirectory().then(({ subDir }) => {
        const names = componentName.split(' ');
        for (let name of names) createNewComponent(name, subDir)
      });
    });
  }
  if (entity === 'entry-point') {
    inputEntryPointName().then(({ entryPointName }) => {
      attachNewOrExistPage().then(({ page }) => {
        if (page === 'Create new page...') {
          inputPageName(entryPointName).then(({ pageName }) => {
            inputSubDirectory().then(({ subDir }) => {
              createNewPage(pageName, subDir);
              createNewEntryPoint(entryPointName, `./src/pages/${subDir}/${pageName}/${pageName}`, subDir);
            });
          });
        } else {
          inputSubDirectory().then(({ subDir }) => {
            createNewEntryPoint(entryPointName, page, subDir);
          });
        }
      })
    })
  }
  if (entity === 'page') {
    inputPageName().then(({ pageName }) => {
      inputSubDirectory().then(({ subDir }) => {
        createNewPage(pageName, subDir)
      });
    });
  }
});

function createNewComponent(name, subDir) {
  const dir = `./src/components/${subDir}/${name}`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
    fs.writeFileSync(`${dir}/${name}.pug`, defineComponentPug(name));
    fs.writeFileSync(`${dir}/${name}.scss`, defineComponentSCSS(name));
    fs.writeFileSync(`${dir}/${name}.js`, defineJS(name));
  }
}

function createNewEntryPoint(entryPointName, pageName, subDir) {
  console.log(`call createNewEntryPoint(${entryPointName},${pageName})`);

  let relativePathToPage = path.relative(`src/entry-points/${subDir}/${entryPointName}`, pageName);
  relativePathToPage = relativePathToPage.replace('.pug', '');

  const dir = `./src/entry-points/${subDir}/${entryPointName}`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
    fs.writeFileSync(`${dir}/${entryPointName}.pug`, defineEntryPointPug(relativePathToPage));
    fs.writeFileSync(`${dir}/${entryPointName}.js`, defineEntryPointJS(relativePathToPage));
  }

}

function createNewPage(name, subDir) {
  const dir = `./src/pages/${subDir}/${name}`;

  if (!fs.existsSync(dir)) {
    fs.mkdirSync(dir, { recursive: true });
    fs.writeFileSync(`${dir}/${name}.pug`, definePug(name));
    fs.writeFileSync(`${dir}/${name}.scss`, defineSCSS(name));
    fs.writeFileSync(`${dir}/${name}.js`, defineJS(name));
  }
}

function chooseEntiryForCreate() {
  return inquirer
    .prompt([
      {
        type: 'list',
        name: 'entity',
        message: 'What do you want to create?',
        choices: ['component', 'entry-point', 'page'],
      },
    ])
}

function attachNewOrExistPage() {
  return inquirer.prompt([{
    type: 'list',
    name: 'page',
    message: 'Create new page or select exist page',
    choices: ['Create new page...', new inquirer.Separator('List of pages:'), ...getExistPageList()],
  }])
}

function getExistPageList() {
  return glob.sync('./src/pages/**/**.pug');
}

function inputComponentName() {
  return inquirer.prompt([{
    type: 'input',
    name: 'componentName',
    message: 'Input name for new component',
  }])
}

function inputEntryPointName() {
  return inquirer.prompt([{
    type: 'input',
    name: 'entryPointName',
    message: 'Input name for new entry-point',
  }])
}

function inputPageName(defaultName) {
  return inquirer.prompt([{
    type: 'input',
    name: 'pageName',
    default: defaultName,
    message: 'Input name for new page',
  }])
}

function inputSubDirectory() {
  return inquirer.prompt([{
    type: 'input',
    name: 'subDir',
    default: '',
    message: 'Input subdirectory, if you don\'t need subdirectory just press enter',
  }])
}
