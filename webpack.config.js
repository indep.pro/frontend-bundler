const path = require('path');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const sass = require('sass');
const PugLintPlugin = require('puglint-webpack-plugin');
const autoprefixer = require('autoprefixer');
const discardduplicates = require('postcss-discard-duplicates');
const flexbugsfixes = require('postcss-flexbugs-fixes');
const {
  getEntryPoints,
  getHtmlEntryPoints,
  removeRootDir,
} = require('./webpack.utils');
const webpack = require('webpack');
const PugPlugin = require('pug-plugin');

module.exports = (_, argv) => {
  const isDev = argv.mode === 'development';
  const dtValue = isDev ? 'source-map' : false;

  return {
    stats: 'errors-warnings',
    devtool: dtValue,
    devServer: {
      historyApiFallback: true,
      open: false,
      compress: false,
      hot: false,
      liveReload: true,
      // port: 8080,
    },
    entry: getEntryPoints(),
    output: {
      path: path.resolve(__dirname, './build'),
      filename: 'js/[name].js',
      clean: true,
      compareBeforeEmit: true,
      assetModuleFilename: (content) => {
        return 'assets/[path][name][ext]';
      },
    },
    optimization: {
      splitChunks: {
        cacheGroups: {
          commons: {
            name: 'commons',
            chunks: 'initial',
            minChunks: 2,
            minSize: 0,
          },
        },
      },
    },
    plugins: [
      new webpack.ProvidePlugin({
        $: 'jquery',
        jQuery: 'jquery',
        'window.jQuery': 'jquery',
      }),
      new PugLintPlugin({
        context: 'src',
        files: '**/*.pug',
        config: require('./.pug-lintrc'),
      }),
      new MiniCssExtractPlugin({
        filename: 'css/[name].css',
        ignoreOrder: true,
      }),
      ...getHtmlEntryPoints().map((page) => {
        const name = path.parse(page).dir.replace('./src/entry-points/', '');

        return new HtmlWebpackPlugin({
          template: path.resolve(__dirname, page),
          filename: `${name}.html`,
          chunks: [path.basename(page, '.pug')],
          inject: 'body',
        });
      }),
    ],
    module: {
      rules: [
        {
          test: /\.pug$/,
          // use: [`pug-loader?pretty=${true}`],
          loader: PugPlugin.loader,
        },
        {
          test: /\.(png|svg|jpg|jpeg|gif|webp)$/i,
          exclude: /(favicons|fonts)/,
          type: 'asset/resource',
          generator: {
            filename: ({ filename }) => `images/${removeRootDir(filename)}`,
          },
        },
        {
          test: /\.(woff|woff2|eot|ttf|otf|svg)$/i,
          include: /(fonts)/,
          type: 'asset/resource',
          generator: {
            filename: ({ filename }) => `fonts/${removeRootDir(filename)}`,
          },
        },
        {
          test: /\.(mp4|m4v|ogg|flv|webm|asf|avi)$/i,
          include: /(components|pages)/,
          type: 'asset/resource',
          generator: {
            filename: ({ filename }) => `video/${removeRootDir(filename)}`,
          },
        },
        {
          test: /\.(mp3|wav|aac|ogg|caf|flac)$/i,
          include: /(components|pages)/,
          type: 'asset/resource',
          generator: {
            filename: ({ filename }) => `audio/${removeRootDir(filename)}`,
          },
        },
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
            options: {
              cacheDirectory: true,
            },
          },
        },
        {
          test: /\.(css|scss)$/,
          use: [
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                publicPath: '../',
              },
            },
            {
              loader: 'css-loader',
              options: {
                sourceMap: true,
                url: true,
              },
            },
            {
              loader: 'postcss-loader',
              options: {
                postcssOptions: {
                  plugins: [
                    autoprefixer(),
                    flexbugsfixes(),
                    discardduplicates(),
                  ],
                },
              },
            },
            {
              loader: 'sass-loader',
              options: { sourceMap: true, implementation: sass },
            },
            {
              loader: 'sass-resources-loader',
              options: {
                resources: ['./src/common.scss'],
              },
            },
          ],
        },
      ],
    },
  };
};
