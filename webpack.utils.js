const glob = require('glob');
const path = require('path');

function getEntryPoints() {
  return glob.sync('./src/entry-points/**/**.js').reduce(function (obj, el) {
    const key = path.parse(el).name;
    if (key !== 'entry-points.utils') obj[key] = el;
    return obj
  }, {})
}

function getHtmlEntryPoints() {
  return glob.sync('./src/entry-points/**/**.pug').reduce(function (obj, el) {
    obj.push(el);
    return obj;
  }, [])
}

function removeRootDir(path) {
  return path.slice(4);
}

module.exports = { getEntryPoints, getHtmlEntryPoints, removeRootDir }
